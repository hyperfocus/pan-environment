Hi,

To start building the reproducible build environment make sure you have the requirements installed.

Requirements:
- A linux machine
- Latest CE docker engine (https://docs.docker.com/engine/installation/)
- Docker compose (https://docs.docker.com/compose/install/)

1. Install requirements above

2. Git clone this repository ```git clone https://gitlab.com/SuperNami/pan-environment.git .```

3. Copy the latest pan files into the ioncube-php-7 folder

4. Build the ioncube-php-7 "files" container by running ```make build-pan-files```

5. Review the environment variables inside the ```docker-compose.yml```

6. Bring the docker containers up by running ```docker-compose up```, wait for the image downloads to complete and afterwards it will start running

7. Change your hosts file to reflect the pan domain ```sudo nano /etc/hosts``` and add ```172.16.234.10 web.revenue.lk```

8. Restore database backup I've sent with for example MySQL workbench, look at login credentials in ```docker-compose.yml```.

9. Either login and run cron manually through the web interface or type ```docker exec -it panenvironment_php_1 bash``` and run the cron script manually inside the container ```/usr/local/bin/php -q /var/www/html/scripts/jobs.php```

10. Now bug will appear, themes will change from gold to green


Thanks for looking into the issues.


Regards,

George