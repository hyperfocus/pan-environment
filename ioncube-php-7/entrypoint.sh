#!/bin/bash

if [ ! -e "/var/www/html/accounts/settings.php" ]; then

  export DB_HOST=${DB_HOST}
  export DB_USER=${DB_USER}
  export DB_PASSWORD=${DB_PASSWORD}
  export DB_NAME=${DB_NAME}

  cp /var/www/html/accounts/default.settings.php /var/www/html/accounts/settings.php
  rm /var/www/html/accounts/default.settings.php

  sed -i "s/hostname/${DB_HOST}/" /var/www/html/accounts/settings.php
  sed -i "s/username/${DB_USER}/" /var/www/html/accounts/settings.php
  sed -i "s/password/${DB_PASSWORD}/" /var/www/html/accounts/settings.php
  sed -i "s/database/${DB_NAME}/" /var/www/html/accounts/settings.php

  echo "settings.php configured"

else
  echo "Credential initialization is already finished"
fi

if [ ! -e "/etc/msmtprc" ]; then

  export ACCOUNT_NAME=${ACCOUNT_NAME}
  export SMTP_SERVER=${SMTP_SERVER}
  export SMTP_PORT=${SMTP_PORT}
  export SMTP_USERNAME=${SMTP_USERNAME}
  export SMTP_PASSWORD=${SMTP_PASSWORD}
  export SMTP_EMAIL=${SMTP_EMAIL}

  touch /etc/msmtprc
  chmod 0600 /etc/msmtprc
  chown www-data:www-data /etc/msmtprc

  echo "defaults" >> /etc/msmtprc
  echo "logfile /var/log/msmtp.log" >> /etc/msmtprc

  echo "tls on" >> /etc/msmtprc
  echo "tls_starttls on" >> /etc/msmtprc
  echo "tls_trust_file /etc/ssl/certs/ca-certificates.crt" >> /etc/msmtprc

  echo "from ${SMTP_EMAIL}" >> /etc/msmtprc
  echo "host ${SMTP_SERVER}" >> /etc/msmtprc
  echo "port ${SMTP_PORT}" >> /etc/msmtprc

  echo "auth login" >> /etc/msmtprc
  echo "user ${SMTP_USERNAME}" >> /etc/msmtprc
  echo "password ${SMTP_PASSWORD}" >> /etc/msmtprc

  echo "account ${ACCOUNT_NAME}" >> /etc/msmtprc
  echo "account default : ${ACCOUNT_NAME}" >> /etc/msmtprc
  echo "sendmail_path = "/usr/local/bin/msmtp -C /etc/msmtprc -a ${ACCOUNT_NAME} -t"" >> /usr/local/etc/php/conf.d/mail.ini

  echo "msmtprc and php configured for email"

else
  echo "MSMTP initialization is already finished"
fi

chown root:root -R /var/www/html/accounts
chmod 777 -R /var/www/html/accounts

exec "$@"